<?php
	Class Login_model extends CI_Model{
		public function __construct(){
            $this->load->database();
        }
		
		public function login($account_number, $PIN){
			$this->db->select('*');
			$this->db->from('details');
			$this->db->where('account_number', $account_number);
			$this->db->where('PIN', MD5($PIN));
			$this->db->limit(1);
			
			$query = $this->db->get();
			
			if($query->num_rows() == 1){
				return $query->result();
			}
			else{
				return false;
			}
		}
		public function def($account_number){
			$this->db->select('*');
			$this->db->from('details');
			$this->db->where('account_number', $account_number);
			$this->db->limit(1);
			
			$query = $this->db->get();
			
			if($query->num_rows() == 1){
				return $query->result();
			}
			else{
				return false;
			}
		}
		public function hist($account_number){
			$this->db->select('*');
			$this->db->from('history');
			$this->db->where('account_number', $account_number);
			$this->db->order_by('id', 'desc');
			$query = $this->db->get();
			
			if($query->num_rows() != 0){
				return $query->result();
			}
			else{
				return false;
			}
		}
		
	}
?>