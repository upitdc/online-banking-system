<html>
		<head>
			<title>Transfer Page</title>
			<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
			<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
			<script type="text/javascript" src="js/bootstrap.min.js"></script>
			<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
			<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
			<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
			<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
				<script type="text/javascript">
				function confirmationfunc(){
					var ask = confirm("Are you sure you want to proceed?");
					if(ask==true){
						//window.location = "deposit.php";
					}
					else{
						//window.location = "home_view.php";
					}
				}
			</script>
        </head>
        <body>
		<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo site_url('login_home')?>">Online Banking System</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo site_url('login_home')?>">Home <span class="sr-only">(current)</span></a></li>
        <li><a href="<?php echo site_url('login_home/depositview')?>">Deposit <span class="sr-only">(current)</span></a></li>
		<li><a href="<?php echo site_url('login_home/withdrawview')?>">Withdraw <span class="sr-only">(current)</span></a></li>
		<li class="active"><a href="<?php echo site_url('login_home/transferview')?>">Transfer <span class="sr-only">(current)</span></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $fname;?>  <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('login_home/logout')?>">Log Out</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav> 
	<div class="container" style="width:100%">
		<div class="jumbotron" style="background-color:white;">
			<h3 style="color:black;text-align:center">Online Banking System</h3>
			<h1 style="color:black;text-align:center">Transfer</h1>
			<br><br>
		</div>
	</div>
<?php
	
	$this->load->library('form_validation');
?>
<?php echo form_open('login_home/transfer'); ?>
<form class="form-horizontal" role="form" method="post">
	<div class="pagination-centered"style="width:70%;margin-left:15%">
		<div class="text-center">
			<label>Enter the account number of the receiver:</label>
			<div class=".col-xs-6 .col-sm-4">
				<input type="text" class="form-control" id="acount_num" name="account_num" placeholder="Account Number">
			</div>
			<p><font size="2" color="red"><?php echo form_error('account_num'); ?></font></p>
		</div>
		<br>
		<div class="text-center">
			<label>Enter the amount to be transferred:</label>
			<div class=".col-xs-6 .col-sm-4">
				<input type="text" class="form-control" id="amount" name="amount" placeholder="Amount">
			</div>
			<p><font size="2" color="red"><?php echo form_error('amount'); ?></font></p>
		</div>
		<br><br>
		<input id="submit" name="submit" type="submit" value="TRANSFER" onclick="return confirm('Are you sure you want to proceed?');" class="btn btn-primary" style="position:relative;background-color:white;border-radius: 24px;color:black;border: 3px solid black;width:200px;font-size:20px;margin-left:calc((100% - 200px)/2);">
	</div>
	<br>
	<br>
	<div class="form-group">
		
	</div>
</form>
<?php $this->load->view('templates/footer')?>
</body>
</html>