<?php
$template = array(
			    'table_open'          => '<table style="text-align: center; border: 0.5px solid white;width:50%; margin-left:25%" >',

                'heading_row_start'   => '<tr style="border: 0.5px solid #b3b3b3; background-color: white; color:black; text-align:center;">',
                'heading_row_end'     => '</tr>',
                'heading_cell_start'  => '<th style="font-size:20px;text-align:center;border: 0.5px solid #f2f2f2;" height=60>',
                'heading_cell_end'    => '</th>',

                'row_start'           => '<tr style="background-color: #f2f2f2;">',
                'row_end'             => '</tr>',
                'cell_start'          => '<td style="font-size:15px;border: 0.5px solid #f2f2f2; text-align=center;" height=40>',
                'cell_end'            => '</td>',

                'row_alt_start'       => '<tr style="background-color: #f2f2f2;">',
                'row_alt_end'         => '</tr>',
                'cell_alt_start'      => '<td style="font-size:15px;border: 0.5px solid #f2f2f2; text-align=center;" height=40>',
                'cell_alt_end'        => '</td>',

                'table_close'         => '</table>'
);

	$this->table->set_heading('History');
	$this->table->set_template($template);
	echo $this->table->generate($records);
	echo '<div id="pagination" style="text-align:center;font-size:20px;height:25px;"><ul>' . $this->pagination->create_links() . '</ul></div>';
?>
<br><br><br><br><br><br>