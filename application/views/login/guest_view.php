<head>
	<link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<title>Welcome Page</title>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<style>
		body{background-image: url("http://umad.com/img/2015/6/skyscraper-wallpaper-hd-8463-8813-hd-wallpapers.jpg");
        background-position: center center;
		background-size: cover;
		}
	</style>
</head>
			<nav class="navbar navbar-inverse" style="position:absolute;font-family: 'Open Sans', sans-serif;left:0px;top:0px;width:100%">
				<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <a class="navbar-brand" href="<?php echo site_url()?>">Online Banking System</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				  <ul class="nav navbar-nav">
					<li class="active"><a href="<?php echo site_url()?>">Home <span class="sr-only">(current)</span></a></li>
				  </ul>
				  <ul class="nav navbar-nav navbar-right">
					<li><a href="">Log-in</a></li>
				  </ul>
				</div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav> 

			<div style="margin-top:10%;width:100%">
					<div class="jumbotron" style="background-color:white;opacity:0.8;">
					<h3 style="font-family: 'Titillium Web', sans-serif;color:black;text-align:center">THIS IS THE BANK</h3>
					<h1 style="color:black;text-align:center;font-family: 'Raleway', sans-serif;">ONLINE BANKING SYSTEM</h1>
					<br><br><br>
					<a type="button" class="btn btn-primary" style="position:relative;background-color:white;border-radius: 24px;color:black;border: 3px solid black;width:200px;font-size:20px;margin-left:calc((100% - 200px)/2);" href="<?php echo site_url('Pages/login')?>">LOG-IN</a>
				</div>
			</div>
			<br><br><br>
			<div class="row">
				<div class="col-sm-4" style="color:black">
					<div style="font-family: 'Titillium Web', sans-serif;position:relative;width:90%;margin-left:5%;height:50%;margin-top:5%;background-color:white;">
						<div style="position:relative;width:90%;margin-left:5%">
						  <br><br><br>
						  <h2>Deposit</h2>
						  <p>You can now deposit with Online Banking System!</p>
						  <p>Users can now deposit using their account. Just log in with your account number and PIN.</p>
						  <br>
						</div>
				  </div>
				</div>
				<div class="col-sm-4" style="color:black">
					<div style="font-family: 'Titillium Web', sans-serif;position:relative;width:90%;margin-left:5%;height:50%;margin-top:5%;background-color:white;">
						<div style="position:relative;width:90%;margin-left:5%">
						  <br><br><br>
						  <h2>Withdraw</h2>
						  <p>You can now withdraw with Online Banking System!</p>
						  <p>Users can now withdraw using their account. Just log in with your account number and PIN.</p>
						  <br>
						</div>
					</div>
				</div>
				<div class="col-sm-4" style="color:black">
					<div style="font-family: 'Titillium Web', sans-serif;position:relative;width:90%;margin-left:5%;height:50%;margin-top:5%;background-color:white;">
						<div style="position:relative;width:90%;margin-left:5%">
						  <br><br><br>
						  <h2>Transfer</h2>
						  <p>You can now transfer money to other accounts with Online Banking System!</p>
						  <p>Users can now transfer money to other accounts. Just log in with your account number and PIN	</p>
						  <br>
						</div>
					</div>
				</div>
			</div>
			<br><br><br>