<html>
 <head>
    <title>Home</title>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<style>
		body{
			background-color:white;
		}
	</style>
 </head>
 <body>
	 <nav class="navbar navbar-inverse" style="position:absolute;font-family: 'Open Sans', sans-serif;left:0px;top:0px;width:100%">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo site_url('login_home')?>">Online Banking System</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="<?php echo site_url('login_home')?>">Home <span class="sr-only">(current)</span></a></li>
        <li><a href="<?php echo site_url('login_home/depositview')?>">Deposit <span class="sr-only">(current)</span></a></li>
		<li><a href="<?php echo site_url('login_home/withdrawview')?>">Withdraw <span class="sr-only">(current)</span></a></li>
		<li><a href="<?php echo site_url('login_home/transferview')?>">Transfer <span class="sr-only">(current)</span></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $fname;?>  <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('login_home/logout')?>">Log Out</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
   <div class="container" style="width:100%;top:200px">
		<div class="jumbotron" style="background-color:white;">
			<h3 style="color:black;text-align:center">ONLINE BANKING SYSTEM</h3>
			<h1 style="color:black;text-align:center">Welcome <?php echo $fname." ".$lname?></h1>
			<br><br>
			<h4 style="color:black;text-align:center">Account Number: <?php echo $account_number; ?></h4>
			<h4 style="color:black;text-align:center">Balance: PHP <?php echo $balance; ?></h4>
			<br><br>
		</div>
	</div>
	
	<?php $this->load->view('templates/footer')?>
   
