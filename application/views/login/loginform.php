<html>
		<head>
			<title>Log-in Page</title>
			<link href='https://fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
			<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
			<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
			<script type="text/javascript" src="js/bootstrap.min.js"></script>
			<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
			<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
			<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
			<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
			<style>
				body{background-image: url("http://randomwallpapers.net/building-sky-cityscape-taipei-taiwan-1920x1200-wallpaper426904.jpg");
				background-position: center center;
				background-size: cover;
				}
			</style>
		</head>
 <body>      
 <nav class="navbar navbar-inverse" style="position:absolute;font-family: 'Open Sans', sans-serif;left:0px;top:0px;width:100%">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo site_url()?>">Online Banking System</a>
    </div>
  </div><!-- /.container-fluid -->
</nav>
<?php $this->load->library('form_validation'); ?>

<?php echo form_open('Pages/verification'); ?>
			<div style="background-color:black;opacity:0.8;width:100%;color:white;margin-top:10%;">
					<div class="jumbotron" style="background-color:transparent !important;width:100%">
					<h1 style="color:white;text-align:center;font-family: 'Poiret One', cursive;">LOG IN</h1>
					<br><br>
					<form class="form-horizontal" role="form" method="post">
						<div class="pagination-centered"style="width:50%;margin-left:25%">
							<div class="text-center">
								<div class=".col-xs-6 .col-sm-4">
									<input type="text" class="form-control" id="account_number" name="account_number" placeholder="Account Number">
								</div>
							<p><font size="2" color="red"><?php echo form_error('account_number'); ?></font></p>
							</div>
							<br>
							<div class="text-center">
								<div class=".col-xs-6 .col-sm-4">
									<input type="password" class="form-control" id="PIN" name="PIN" placeholder="PIN">
								</div>
							<p><font size="2" color="red"><?php echo form_error('PIN'); ?></font></p></br>
							</div>
							<br><br>
							<input id="submit" name="submit" type="submit" value="LOGIN"  class="btn btn-primary" style="position:relative;background-color:black;border-radius: 24px;color:white;border: 3px solid white;width:200px;font-size:20px;margin-left:calc((100% - 200px)/2);">
						</div>
						<br>
						<br>
					</form>
				</div>
			</div>
			<br><br><br>
			<?php $this->load->view('templates/footer');?>
</body>
</html>