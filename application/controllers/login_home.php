<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class login_home extends CI_Controller {
 
 public function __construct()
 {
   parent::__construct();
   $this->load->model('login_model','',TRUE);
   $this->load->database();
 }
 
 function index()
 {
		if($this->session->userdata('logged_in'))
		{
		$session_data = $this->session->userdata('logged_in');
		$data['account_number'] = $session_data['account_number'];
		$data['fname'] = $session_data['fname'];
		$data['lname'] = $session_data['lname'];
		$data['balance'] = $session_data['balance'];
		$result = $this->login_model->hist($data['account_number']);
		if($result){
			$this->load->library('pagination');
			$histdata['base_url'] = site_url().'/login_home/index/';
			$this->db->select('entry');
			$this->db->where('account_number', $data['account_number']);
			$histdata['total_rows'] = $this->db->get('history')->num_rows();
			$histdata['per_page'] = 15;
			$histdata['num_links'] = 5;
			$histdata['full_tag_open'] = "<ul class='pagination'>";
			$histdata['full_tag_close'] ="</ul>";
			$histdata['num_tag_open'] = '<li>';
			$histdata['num_tag_close'] = '</li>';
			$histdata['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$histdata['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$histdata['next_tag_open'] = "<li>";
			$histdata['next_tagl_close'] = "</li>";
			$histdata['prev_tag_open'] = "<li>";
			$histdata['prev_tagl_close'] = "</li>";
			$histdata['first_tag_open'] = "<li>";
			$histdata['first_tagl_close'] = "</li>";
			$histdata['last_tag_open'] = "<li>";
			$histdata['last_tagl_close'] = "</li>";
			$histdata['first_link'] = 'First';
			$histdata['last_link'] = 'Last';
			$histdata['prev_link'] = 'Prev';
			$histdata['next_link'] = 'Next';
			$this->db->select('entry');
			$this->db->where('account_number', $data['account_number']);
			$this->db->order_by('id' , 'desc');
			$query = $this->db->get('history', $histdata['per_page'], $this->uri->segment(3));
			$histdata['records'] = $query;
			$this->pagination->initialize($histdata);
			echo "<br><br>";
			$this->load->view('login/home_view', $data);
			$this->load->view('login/history', $histdata);
		}
		else{
			echo "<br><br>";
			$this->load->view('login/home_view', $data);
			$this->load->view('login/no_his');
		}
   }
   else
   {
		 //If no session, redirect to login page
		echo "session problem";
   }
 }
 function depositview(){
	if($this->session->userdata('logged_in'))
   {
		$session_data = $this->session->userdata('logged_in');
		$data['fname'] = $session_data['fname'];
		$this->load->view('login/deposit', $data);
   }
   else
   {
     //If no session, redirect to login page
    echo "session problem";
   }
 }
 function deposit(){
	 $this->load->library('form_validation');
 
   $this->form_validation->set_rules('amount', 'Amount', 'required|is_numeric|callback_checkneg');
 
   if($this->form_validation->run() == FALSE)
   {
     //Field validation failed.  User redirected to login page
	$session_data = $this->session->userdata('logged_in');
	$data['account_number'] = $session_data['account_number'];
	$data['fname'] = $session_data['fname'];
	$this->load->view('login/deposit', $data);
   }
   else
   {
	  if($this->session->userdata('logged_in'))
		   {
			 $additional = $this->input->post('amount');
			 $session_data = $this->session->userdata('logged_in');
			 $data['account_number'] = $session_data['account_number'];
			 $data['fname'] = $session_data['fname'];
			 $data['lname'] = $session_data['lname'];
			 $result = $this->login_model->def($data['account_number']);
			 if($result)
				{
					foreach($result as $row)
					 {
					   $temporary = array(
						 'account_number' => $row->account_number,
						 'fname' => $row->fname,
						 'lname' => $row->lname,
						 'balance' => $row->balance
					   );
					 }
					$data['balance'] = $temporary['balance'];
					$data['balance'] = $data['balance'] + $additional;
					$this->db->where('account_number', $data['account_number']);
					$this->db->update('details', $data);
			 
				}
				$this->load->helper('date');
				$date = time()+54000;
				$history['id'] = NULL;
				$history['account_number'] = $data['account_number'];
				$history['entry'] =  " Deposited PHP ".$additional." to own account (".date("Y-m-d H:i:s", $date).")";
				$this->db->insert('history', $history);
				$result = $this->login_model->hist($data['account_number']);
				if($result){
					$this->load->library('pagination');
					$histdata['base_url'] = site_url().'/login_home/index/';
					$this->db->select('entry');
					$this->db->where('account_number', $data['account_number']);
					$histdata['total_rows'] = $this->db->get('history')->num_rows();
					$histdata['per_page'] = 15;
					$histdata['num_links'] = 5;
					$histdata['full_tag_open'] = "<ul class='pagination'>";
					$histdata['full_tag_close'] ="</ul>";
					$histdata['num_tag_open'] = '<li>';
					$histdata['num_tag_close'] = '</li>';
					$histdata['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
					$histdata['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
					$histdata['next_tag_open'] = "<li>";
					$histdata['next_tagl_close'] = "</li>";
					$histdata['prev_tag_open'] = "<li>";
					$histdata['prev_tagl_close'] = "</li>";
					$histdata['first_tag_open'] = "<li>";
					$histdata['first_tagl_close'] = "</li>";
					$histdata['last_tag_open'] = "<li>";
					$histdata['last_tagl_close'] = "</li>";
					$histdata['first_link'] = 'First';
					$histdata['last_link'] = 'Last';
					$histdata['prev_link'] = 'Prev';
					$histdata['next_link'] = 'Next';
					$this->db->select('entry');
					$this->db->where('account_number', $data['account_number']);
					$this->db->order_by('id' , 'desc');
					$query = $this->db->get('history', $histdata['per_page'], $this->uri->segment(3));
					$histdata['records'] = $query;
					$this->pagination->initialize($histdata);
					echo "<br><br>";
					$this->load->view('login/home_view', $data);
					$this->load->view('login/history', $histdata);
				}
				else{
					echo "<br><br>";
					$this->load->view('login/home_view', $data);
					$this->load->view('login/no_his');
				}
		   }
		   else
		   {
			 //If no session, redirect to login page
			echo "session problem";
		   }
		}
	}

 function withdrawview(){
	if($this->session->userdata('logged_in'))
   {
		$session_data = $this->session->userdata('logged_in');
		$data['fname'] = $session_data['fname'];
		$this->load->view('login/withdraw', $data);
   }
   else
   {
     //If no session, redirect to login page
    echo "session problem";
   }
 }
 function withdraw(){
	 $this->load->library('form_validation');
 
	   $this->form_validation->set_rules('amount', 'Amount', 'required|is_numeric|callback_checkwith|callback_checkneg');
	 
	   if($this->form_validation->run() == FALSE)
	   {
		 //Field validation failed.  User redirected to login page
		$session_data = $this->session->userdata('logged_in');
		$data['account_number'] = $session_data['account_number'];
		$data['fname'] = $session_data['fname'];
		$this->load->view('login/withdraw', $data);
	   }
	  else{
	  if($this->session->userdata('logged_in'))
		   {
			 $sub = $this->input->post('amount');
			 $session_data = $this->session->userdata('logged_in');
			 $data['account_number'] = $session_data['account_number'];
			 $data['fname'] = $session_data['fname'];
			 $data['lname'] = $session_data['lname'];
			 $result = $this->login_model->def($data['account_number']);
			 if($result)
				{
					foreach($result as $row)
					 {
					   $temporary = array(
						 'account_number' => $row->account_number,
						 'fname' => $row->fname,
						 'lname' => $row->lname,
						 'balance' => $row->balance
					   );
					 }
					$data['balance'] = $temporary['balance'];
					$data['balance'] = $data['balance'] - $sub;
					$this->db->where('account_number', $data['account_number']);
					$this->db->update('details', $data);
				}
				$this->load->helper('date');
				$date = time()+10800;
				$history['id'] = NULL;
				$history['account_number'] = $data['account_number'];
				$history['entry'] =  " Withdrew PHP ".$sub." from own account (".date("Y-m-d H:i:s", $date).")";
				$this->db->insert('history', $history);
				$result = $this->login_model->hist($data['account_number']);
				if($result){
					$this->load->library('pagination');
					$histdata['base_url'] = site_url().'/login_home/index/';
					$this->db->select('entry');
					$this->db->where('account_number', $data['account_number']);
					$histdata['total_rows'] = $this->db->get('history')->num_rows();
					$histdata['per_page'] = 15;
					$histdata['num_links'] = 5;
					$histdata['full_tag_open'] = "<ul class='pagination'>";
					$histdata['full_tag_close'] ="</ul>";
					$histdata['num_tag_open'] = '<li>';
					$histdata['num_tag_close'] = '</li>';
					$histdata['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
					$histdata['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
					$histdata['next_tag_open'] = "<li>";
					$histdata['next_tagl_close'] = "</li>";
					$histdata['prev_tag_open'] = "<li>";
					$histdata['prev_tagl_close'] = "</li>";
					$histdata['first_tag_open'] = "<li>";
					$histdata['first_tagl_close'] = "</li>";
					$histdata['last_tag_open'] = "<li>";
					$histdata['last_tagl_close'] = "</li>";
					$histdata['first_link'] = 'First';
					$histdata['last_link'] = 'Last';
					$histdata['prev_link'] = 'Prev';
					$histdata['next_link'] = 'Next';
					$this->db->select('entry');
					$this->db->where('account_number', $data['account_number']);
					$this->db->order_by('id' , 'desc');
					$query = $this->db->get('history', $histdata['per_page'], $this->uri->segment(3));
					$histdata['records'] = $query;
					$this->pagination->initialize($histdata);
					echo "<br><br>";
					$this->load->view('login/home_view', $data);
					$this->load->view('login/history', $histdata);
				}
				else{
					echo "<br><br>";
					$this->load->view('login/home_view', $data);
					$this->load->view('login/no_his');
				}

		   }
		   else
		   {
			 //If no session, redirect to login page
			echo "session problem";
		   }
	  }
 }
 function transferview(){
	if($this->session->userdata('logged_in'))
   {
		$session_data = $this->session->userdata('logged_in');
		$data['fname'] = $session_data['fname'];
		$this->load->view('login/transfer', $data);
   }
   else
   {
     //If no session, redirect to login page
    echo "session problem";
   }
 }
 function transfer(){
	 $this->load->library('form_validation');
	   $this->form_validation->set_rules('account_num', 'Account Number', 'required|callback_checklen|callback_checkexist');
	   $this->form_validation->set_rules('amount', 'Amount', 'required|is_numeric|callback_checktrans|callback_checkneg');
	 if($this->form_validation->run() == FALSE)
	   {
		 //Field validation failed.  User redirected to login page
		$session_data = $this->session->userdata('logged_in');
		$data['account_number'] = $session_data['account_number'];
		$data['fname'] = $session_data['fname'];
		$this->load->view('login/transfer', $data);
	   }
	  else{
	  if($this->session->userdata('logged_in'))
		   {
			 $account_num = $this->input->post('account_num');
			 $amt = $this->input->post('amount');
			 $session_data = $this->session->userdata('logged_in');
			 $data['account_number'] = $session_data['account_number'];
			 $data['fname'] = $session_data['fname'];
			 $data['lname'] = $session_data['lname'];
			 $sender = $this->login_model->def($data['account_number']);
			 $receiver = $this->login_model->def($account_num);
			 if($sender)
				{
					foreach($sender as $row)
					 {
					   $temporary1 = array(
						 'account_number' => $row->account_number,
						 'fname' => $row->fname,
						 'lname' => $row->lname,
						 'balance' => $row->balance
					   );
					 }
					$data['balance'] = $temporary1['balance'];
					$data['balance'] = $data['balance'] - $amt;
					$this->db->where('account_number', $data['account_number']);
					$this->db->update('details', $data);
				}
			 if($receiver)
				{
					foreach($receiver as $row)
					 {
					   $temporary2 = array(
						 'account_number' => $row->account_number,
						 'fname' => $row->fname,
						 'lname' => $row->lname,
						 'balance' => $row->balance
					   );
					 }
					$temporary2['balance'] = $temporary2['balance'] + $amt;
					$this->db->where('account_number', $temporary2['account_number']);
					$this->db->update('details', $temporary2);
			 
				}
				$this->load->helper('date');
				$date = time()+10800;
				$history['id'] = NULL;
				$history['account_number'] = $data['account_number'];
				$history['entry'] =  " Transfered PHP ".$amt." to ".$temporary2['fname']." ".$temporary2['lname']." (".date("Y-m-d H:i:s", $date).")";
				$this->db->insert('history', $history);
				$result = $this->login_model->hist($data['account_number']);
				if($result){
					$this->load->library('pagination');
					$histdata['base_url'] = site_url().'/login_home/index/';
					$this->db->select('entry');
					$this->db->where('account_number', $data['account_number']);
					$histdata['total_rows'] = $this->db->get('history')->num_rows();
					$histdata['per_page'] = 15;
					$histdata['num_links'] = 5;
					$histdata['full_tag_open'] = "<ul class='pagination'>";
					$histdata['full_tag_close'] ="</ul>";
					$histdata['num_tag_open'] = '<li>';
					$histdata['num_tag_close'] = '</li>';
					$histdata['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
					$histdata['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
					$histdata['next_tag_open'] = "<li>";
					$histdata['next_tagl_close'] = "</li>";
					$histdata['prev_tag_open'] = "<li>";
					$histdata['prev_tagl_close'] = "</li>";
					$histdata['first_tag_open'] = "<li>";
					$histdata['first_tagl_close'] = "</li>";
					$histdata['last_tag_open'] = "<li>";
					$histdata['last_tagl_close'] = "</li>";
					$histdata['first_link'] = 'First';
					$histdata['last_link'] = 'Last';
					$histdata['prev_link'] = 'Prev';
					$histdata['next_link'] = 'Next';
					$this->db->select('entry');
					$this->db->where('account_number', $data['account_number']);
					$this->db->order_by('id' , 'desc');
					$query = $this->db->get('history', $histdata['per_page'], $this->uri->segment(3));
					$histdata['records'] = $query;
					$this->pagination->initialize($histdata);
					echo "<br><br>";
					$this->load->view('login/home_view', $data);
					$this->load->view('login/history', $histdata);
				}
				else{
					echo "<br><br>";
					$this->load->view('login/home_view', $data);
					$this->load->view('login/no_his');
				}

		   }
		   else
		   {
			 //If no session, redirect to login page
			echo "session problem";
		   }
	  }
 }
  function his($account_number){
			$this->db->select('*');
			$this->db->from('history');
			$this->db->where('account_number', $account_number);
			$this->db->limit(1);
			
			$query = $this->db->get();
			
			if($query->num_rows() == 1){
				return $query->result();
			}
			else{
				return false;
			}
		}
  function checkwith(){
	 $session_data = $this->session->userdata('logged_in');
	 $result = $this->login_model->def($session_data['account_number']);
	 if($result){
		foreach($result as $row)
					 {
					   $temporary = $row->balance;
					 }
		$sub = $this->input->post('amount');
		$value = $temporary - $sub;
	 }
	 if($value>=0){
		 return true;
	 }
	 else{
		 $this->form_validation->set_message('checkwith', 'You do not have enough balance to complete the transaction. Please try again.');
		 return false;
	 }
 }
 function checktrans(){
	 $value = 0;
	 $session_data = $this->session->userdata('logged_in');
	 $result = $this->login_model->def($session_data['account_number']);
	 if($result){
		foreach($result as $row)
					 {
					   $temporary = $row->balance;
					 }
		$sub = $this->input->post('amount');
		$value = $temporary - $sub;
	 }
	if($value>=0){
		 return true;
	 }
	 else{
		 $this->form_validation->set_message('checktrans', 'You do not have enough balance to complete the transaction. Please try again.');
		 return false;
	 }
 }
  function checkneg(){
		$amount = $this->input->post('amount');
		if($amount<=0){
			$this->form_validation->set_message('checkneg', 'You entered an invalid amount. Please try again.');
			return false;
		}
		else{
			return true;
		}
	}
	 function checklen(){
	$account = $this->input->post('account_num');
	$length = strlen($account);
	if($length>10){
		$this->form_validation->set_message('checklen', 'Error: You entered more than 10 digits. Please try again.');
		return false;
	}
	else if($length<10){
		$this->form_validation->set_message('checklen', 'Error: You entered less than 10 digits. Please try again.');
		return false;
	}
	else{
		return true;
	}
 }
  function checkexist(){
	 $receiver = $this->input->post('account_num');
	 $result = $this->login_model->def($receiver);
	 if(!$result){
		 $this->form_validation->set_message('checkexist', 'Error: Account does not exist. Please try again.');
		return false;
	 }
	 else{
		 return true;
	 }
 }
 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('Pages/login', 'refresh');
 }
}

?>