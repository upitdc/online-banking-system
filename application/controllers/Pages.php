<html>

<body >
<?php
class Pages extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			$this->load->model('login_model','',TRUE);
		}
        public function index($page = 'home')
        {
			if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
			{
					// Whoops, we don't have a page for that!
					show_404();
			}	
			if($this->session->userdata('logged_in'))
			{
				$session_data = $this->session->userdata('logged_in');
				$data['account_number'] = $session_data['account_number'];
				$data['fname'] = $session_data['fname'];
				$data['lname'] = $session_data['lname'];
				$data['balance'] = $session_data['balance'];
				$result = $this->login_model->hist($data['account_number']);
				$this->load->library('pagination');
				$histdata['base_url'] = site_url().'/login_home/index/';
				$this->db->select('entry');
				$this->db->where('account_number', $data['account_number']);
				$histdata['total_rows'] = $this->db->get('history')->num_rows();
				$histdata['per_page'] = 15;
				$histdata['num_links'] = 5;
				$histdata['full_tag_open'] = "<ul class='pagination'>";
				$histdata['full_tag_close'] ="</ul>";
				$histdata['num_tag_open'] = '<li>';
				$histdata['num_tag_close'] = '</li>';
				$histdata['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
				$histdata['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
				$histdata['next_tag_open'] = "<li>";
				$histdata['next_tagl_close'] = "</li>";
				$histdata['prev_tag_open'] = "<li>";
				$histdata['prev_tagl_close'] = "</li>";
				$histdata['first_tag_open'] = "<li>";
				$histdata['first_tagl_close'] = "</li>";
				$histdata['last_tag_open'] = "<li>";
				$histdata['last_tagl_close'] = "</li>";
				$histdata['first_link'] = 'First';
				$histdata['last_link'] = 'Last';
				$histdata['prev_link'] = 'Prev';
				$histdata['next_link'] = 'Next';				
				$this->db->select('entry');
				$this->db->where('account_number', $data['account_number']);
				$this->db->order_by('id' , 'desc');
				$query = $this->db->get('history', $histdata['per_page'], $this->uri->segment(3));
				$histdata['records'] = $query;
				$this->pagination->initialize($histdata);
				$this->load->view('login/home_view', $data);
				$this->load->view('login/history', $histdata);
			}
			else{
				$this->load->view('login/guest_view');
			}
			$this->load->view('templates/footer');
		}
			public function login(){
				$this->load->helper(array('form'));
				$this->load->view('login/loginform');
			}
			
			public function verification()
			 {
			   //This method will have the credentials validation
			   $this->load->library('form_validation');
			 
			   $this->form_validation->set_rules('account_number', 'Account Number', 'required|callback_checklen');
			   $this->form_validation->set_rules('PIN', 'PIN', 'required|callback_check_database');
			 
			   if($this->form_validation->run() == FALSE)
			   {
				 //Field validation failed.  User redirected to login page
				$this->load->view('login/loginform');
			   }
			   else
			   {
				 redirect('login_home', 'refresh');
			   }
			 
			 }
			 
			 public function check_database($PIN)
			 {
			   //Field validation succeeded.  Validate against database
			   $account_number = $this->input->post('account_number');
			 
			   //query the database
			   $result = $this->login_model->login($account_number, $PIN);
			 
			   if($result)
			   {
				 $sess_array = array();
				 foreach($result as $row)
				 {
				   $sess_array = array(
					 'account_number' => $row->account_number,
					 'fname' => $row->fname,
					 'lname' => $row->lname,
					 'balance' => $row->balance
				   );
				   $this->session->set_userdata('logged_in', $sess_array);
				 }
				 return true;
			   }
			   else
			   {
				 $this->form_validation->set_message('check_database', 'Invalid account number or PIN');
				 return false;
			   }
			 }
			 
			 public function checklen(){
				$account = $this->input->post('account_number');
				$length = strlen($account);
				if($length>10){
					$this->form_validation->set_message('checklen', 'Error: You entered more than 10 digits. Please try again.');
					return false;
				}
				else if($length<10){
					$this->form_validation->set_message('checklen', 'Error: You entered less than 10 digits. Please try again.');
					return false;
				}
				else{
					return true;
				}
			 }
		}
			 
			?>
			<br><br>
</body>
</html>